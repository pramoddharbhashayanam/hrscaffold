var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "defaultLanguage" : "en",
  "displayName" : "CompanyDashboard",
  "homePage" : "Main",
  "name" : "CompanyDashboard",
  "platformType" : "WEB",
  "securityEnabled" : "true",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};