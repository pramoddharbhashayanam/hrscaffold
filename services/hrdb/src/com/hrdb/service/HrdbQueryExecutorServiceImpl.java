/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.hrdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hrdb.models.query.*;

@Service
public class HrdbQueryExecutorServiceImpl implements HrdbQueryExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HrdbQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("hrdbWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<CheckingResponse> executeChecking(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("checking", params, CheckingResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportChecking(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("checking", params, exportType, CheckingResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmploteeListResponse> executeEmploteeList(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmploteeList", params, EmploteeListResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmploteeList(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmploteeList", params, exportType, EmploteeListResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeSelectVarResponse> executeEmployeeSelectVar(Integer selectedEmployee, Pageable pageable) {
        Map params = new HashMap(1);

        params.put("SelectedEmployee", selectedEmployee);

        return queryExecutor.executeNamedQuery("EmployeeSelectVar", params, EmployeeSelectVarResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeSelectVar(ExportType exportType, Integer selectedEmployee, Pageable pageable) {
        Map params = new HashMap(1);

        params.put("SelectedEmployee", selectedEmployee);

        return queryExecutor.exportNamedQueryData("EmployeeSelectVar", params, exportType, EmployeeSelectVarResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<SearchEmployeesResponse> executeSearchEmployees(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("SearchEmployees", params, SearchEmployeesResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportSearchEmployees(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("SearchEmployees", params, exportType, SearchEmployeesResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<SelectEmployeeResponse> executeSelectEmployee(Integer searchId, Integer liveListId, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("SearchID", searchId);
        params.put("LiveListID", liveListId);

        return queryExecutor.executeNamedQuery("SelectEmployee", params, SelectEmployeeResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportSelectEmployee(ExportType exportType, Integer searchId, Integer liveListId, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("SearchID", searchId);
        params.put("LiveListID", liveListId);

        return queryExecutor.exportNamedQueryData("SelectEmployee", params, exportType, SelectEmployeeResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeListOfDeptId2Response> executeEmployeeListOf_DeptId2(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmployeeListOf_DeptId2", params, EmployeeListOfDeptId2Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeListOf_DeptId2(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmployeeListOf_DeptId2", params, exportType, EmployeeListOfDeptId2Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeListOfDeptid3Response> executeEmployeeListOf_Deptid3(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmployeeListOf_Deptid3", params, EmployeeListOfDeptid3Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeListOf_Deptid3(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmployeeListOf_Deptid3", params, exportType, EmployeeListOfDeptid3Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeListOfDeptId1Response> executeEmployeeListOf_DeptId1(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmployeeListOf_DeptId1", params, EmployeeListOfDeptId1Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeListOf_DeptId1(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmployeeListOf_DeptId1", params, exportType, EmployeeListOfDeptId1Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<DepartmentCountResponse> executeDepartmentCount(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("DepartmentCount", params, DepartmentCountResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportDepartmentCount(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("DepartmentCount", params, exportType, DepartmentCountResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<SrvVarEmployeeCountResponse> executeSrv_Var_EmployeeCount(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("Srv_Var_EmployeeCount", params, SrvVarEmployeeCountResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportSrv_Var_EmployeeCount(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("Srv_Var_EmployeeCount", params, exportType, SrvVarEmployeeCountResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<ColleagueDetailsResponse> execute_ColleagueDetails(Integer selectedemployeedeptid, Integer selectedemployeeid, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("selectedemployeedeptid", selectedemployeedeptid);
        params.put("selectedemployeeid", selectedemployeeid);

        return queryExecutor.executeNamedQuery("_ColleagueDetails", params, ColleagueDetailsResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable export_ColleagueDetails(ExportType exportType, Integer selectedemployeedeptid, Integer selectedemployeeid, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("selectedemployeedeptid", selectedemployeedeptid);
        params.put("selectedemployeeid", selectedemployeeid);

        return queryExecutor.exportNamedQueryData("_ColleagueDetails", params, exportType, ColleagueDetailsResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<SelectEmployeeVarResponse> executeSelectEmployeeVar(Integer searchId, Integer liveListId, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("SearchID", searchId);
        params.put("LiveListID", liveListId);

        return queryExecutor.executeNamedQuery("SelectEmployeeVar", params, SelectEmployeeVarResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportSelectEmployeeVar(ExportType exportType, Integer searchId, Integer liveListId, Pageable pageable) {
        Map params = new HashMap(2);

        params.put("SearchID", searchId);
        params.put("LiveListID", liveListId);

        return queryExecutor.exportNamedQueryData("SelectEmployeeVar", params, exportType, SelectEmployeeVarResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeListOfDeptId4Response> executeEmployeeListOf_DeptId4(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmployeeListOf_DeptId4", params, EmployeeListOfDeptId4Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeListOf_DeptId4(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmployeeListOf_DeptId4", params, exportType, EmployeeListOfDeptId4Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<NoOfDepartmentResponse> executeNoOfDepartment(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("NoOfDepartment", params, NoOfDepartmentResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportNoOfDepartment(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("NoOfDepartment", params, exportType, NoOfDepartmentResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<TotalBudgetResponse> executeTotalBudget(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("TotalBudget", params, TotalBudgetResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportTotalBudget(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("TotalBudget", params, exportType, TotalBudgetResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Page<EmployeeListOfDeptId5Response> executeEmployeeListOf_DeptId5(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("EmployeeListOf_DeptId5", params, EmployeeListOfDeptId5Response.class, pageable);
    }

    @Transactional(readOnly = true, value = "hrdbTransactionManager")
    @Override
    public Downloadable exportEmployeeListOf_DeptId5(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("EmployeeListOf_DeptId5", params, exportType, EmployeeListOfDeptId5Response.class, pageable);
    }

}


